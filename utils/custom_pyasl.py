from PyAstronomy import pyasl
from PyAstronomy.pyaC import pyaPermanent as pp
import os
import numpy as np
import six
import csv
import gzip

from .table_edited import *

class NasaExoplanetArchive(pyasl.NasaExoplanetArchive):

    def __init__(self, skipUpdate=False):
        self.data = None
        self.dataFileName = os.path.join("pyasl", "resBased", "NEXA.csv.gz")
        configFileName = os.path.join("pyasl", "resBased", "NEXA.cfg")
        pp.PyAUpdateCycle.__init__(self, configFileName, "NEXA")
        # Define columns to select
        # Column name, Description, Unit
        temp_columns = list(range(21))
        temp_columns[0] = ["pl_hostname", "Name of host star", "", "U15"]
        temp_columns[1] = ["pl_name", "Name of the planet", "", "U15"]
        temp_columns[2] = ["pl_letter",
                            "Planet letter (e.g., b, c, d, etc.)", "", "U2"]
        temp_columns[3] = ["ra", "Right ascension", "deg", np.float]
        temp_columns[4] = ["dec", "Declination", "deg", np.float]
        temp_columns[5] = ["pl_orbper",
                            "Planetary orbital period", "d", np.float]
        temp_columns[6] = ["pl_massj", "Planetary mass", "MJ", np.float]
        temp_columns[7] = ["pl_radj", "Planetary radius", "RJ", np.float]
        temp_columns[8] = ["pl_trandep",
                            "Central depth of transit", "%", np.float]
        temp_columns[9] = ["pl_trandur", "Transit duration", "d", np.float]
        temp_columns[10] = ["pl_tranmid", "Transit midpoint", "BJD", np.float]
        temp_columns[11] = ["pl_orbsmax", "Semi-major-axis", "AU", np.float]
        temp_columns[12] = ["pl_orbincl",
                             "Orbital inclination of planet", "deg", np.float]
        temp_columns[13] = ["st_rad", "Stellar radii", "Solar", np.float]
        temp_columns[14] = ["st_dist", "Distance to star", "pc", np.float]
        temp_columns[15] = ["st_mass", "Stellar mass", "Solar", np.float]
        temp_columns[16] = ["st_teff",
                             "Effective temperature of star", "K", np.float]
        temp_columns[17] = ["st_vsini", "Stellar vsin(i)", "km/s", np.float]
        temp_columns[18] = ["st_logg",
                             "Stellar surface gravity", "cm/s**2", np.float]
        temp_columns[19] = ["st_acts", "Stellar S-Index", "", np.float]
        temp_columns[20] = [
            "st_vj", "Stellar V-band brightness", "mag", np.float]

        # =====================================================================
        # Edit by Antoine DB
        # Add columns to table.
        # Go to https://exoplanetarchive.ipac.caltech.edu/docs/API_exoplanet_columns.html#planetcol
        # to get the columns' keys
        # The simply append it to temp_columns as shown below
        # =====================================================================

        temp_columns.insert(8,["pl_radjerr1", "Planetary radius error +", "RJ", np.float])
        temp_columns.insert(9,["pl_radjerr2", "Planetary radius error -", "RJ", np.float])

        temp_columns.insert(7,["pl_massjerr1", "Planetary mass error +", "MJ", np.float])
        temp_columns.insert(8,["pl_massjerr2", "Planetary mass error -", "MJ", np.float])

        temp_columns.append(["st_j","J-band (2MASS)","mag",np.float])
        temp_columns.append(["st_h","H-band (2MASS)","mag",np.float])
        temp_columns.append(["st_k","Ks-band (2MASS)","mag",np.float])
        temp_columns.append(["pl_eqt","Equilibirum Temperature","K",np.float])
        temp_columns.append(["pl_tranflag","Planet Transit Flag","",np.int])
        temp_columns.append(["pl_imppar","Impact parameter","",np.float])
        temp_columns.append(["st_spstr","Spectral type","","U15"])
        temp_columns.append(["st_plx","Parallax","mas",np.float])
        temp_columns.append(["st_radv","Radial velocity","km/s",np.float])


        self._columns = dict(enumerate(temp_columns))

        # ==========================
        # End of edit
        # ==========================

        # Check whether data file exists
        self._fs = pp.PyAFS()

        if not skipUpdate and self.needsUpdate():
            # Data needs update
            print("Downloading exoplanet data from NASA exoplanet archive")
            self._update(self._downloadData)
            print("Saved data to file: ",
                  self.dataFileName, " in data directory,")
            print("  which has been configured as: ", self._fs.dpath)
            print("By default, the data will be downloaded anew every 7 days.")
            print("You can use the `changeDownloadCycle` to change this behavior.")
        self._readData()


def get_nasa_and_eu(skipUpdate=False):
    """
    Get both nasa archive and exoplanet.eu and merge them. Sometimes
    data are not consistant between the two.
    """

    # Get data from catalogs

    exo_eu = pyasl.ExoplanetEU2(skipUpdate=skipUpdate)
    dat_eu = Table(exo_eu.getAllDataAPT())

    nasa = NasaExoplanetArchive(skipUpdate=skipUpdate)
    dat_nasa = Table(nasa.getAllData(),masked=True)
    for _,[col,_,unit,_] in nasa._columns.items():
        dat_nasa[col].unit = unit

    # Change data type of dat_eu['name']
    temp = MaskedColumn(dat_eu['name'],dtype='S15')
    del dat_eu['name']
    dat_eu.add_column(temp, index=0)

    # Convert nan to mask
    dat_nasa.nan_to_mask()
    dat_eu.nan_to_mask()

    # Complete EU database with nasa database ------
    # (EU values are chosen if both available)

    nasa_2_join = Table(dat_nasa[
                        'pl_name',
                        'pl_orbper',
                        'pl_massj','pl_massjerr1','pl_massjerr2',
                        'pl_radj','pl_radjerr1','pl_radjerr2',
                        'pl_imppar',
                        'pl_eqt',
                        'pl_tranflag',
                        'pl_trandur',
                        'pl_tranmid',
                        'pl_orbsmax',
                        'pl_orbincl',
                        'st_rad',
                        'st_dist',
                        'st_mass',
                        'st_spstr',
                        'st_vsini',
                        'st_radv',
                        'st_logg',
                        'st_plx',
                        'st_teff',
                        'st_j','st_h','st_k'])

    nasa_2_join.rename_columns([
                'pl_name',
                'pl_orbper',
                'pl_massj','pl_massjerr1','pl_massjerr2',
                'pl_radj','pl_radjerr1','pl_radjerr2',
                'pl_imppar',
                'pl_eqt',
                'pl_tranmid',
                'pl_orbsmax',
                'pl_orbincl',
                'st_rad',
                'st_dist',
                'st_mass',
                'st_spstr',
                'st_teff',
                'st_j','st_h','st_k'],

                ['name',
                'orbital_period',
                'mass','mass_err_max','mass_err_min',
                'radius','radius_err_max','radius_err_min',
                'impact_parameter',
                'temp_calculated',
                't_zero_tr',
                'semi_major_axis',
                'inclination',
                'star_radius',
                'star_distance',
                'star_mass',
                'star_sp_type',
                'star_teff',
                'mag_j','mag_h','mag_k'])

    rename_str(dat_eu,'_error_','_err_')

    dat_eu = dat_eu.complete(nasa_2_join,key='name')

    # Add calculated equilibrium temperature with nul albedo
    col = MaskedColumn(name='temp_inso',unit='K',
                   description='Insolation temperature',data=Tinso(dat_eu))
    dat_eu.add_column(col)

    return dat_eu


def rename_str(data,old,new):

    for key in data.keys():
        new_key = key.replace(old,new)
        if new_key != key:
            data[key].name = key.replace(old,new)

def Tinso(dat,T='star_teff',R='star_radius',a='semi_major_axis'):
    Tstar = dat[T]
    Rstar = dat[R]
    d = dat[a].to(dat[R].unit)
    return Tstar * np.sqrt(Rstar/2/d)

def b_calc(a,i,R_star):

    out = (a / R_star * np.cos(i.to('rad'))).decompose()
    if not out.unit:
        raise UnitConversionError('b must not have any unit')
    return out
