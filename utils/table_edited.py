from astropy.table import join
import astropy.table as table
from astropy.table.operations import _join, _merge_table_meta
import numpy as np


class MaskedColumn(table.MaskedColumn):

    def find(self, sub, start=0, end=None):
        if isinstance(self, (MaskedColumn)):
            str_array = np.array(self, dtype=str)
            index = np.core.defchararray.find(str_array, sub, start=start, end=end) != -1
            return np.where(index)[0], str_array[index]

        else:
            return NotImplemented


class Column(table.Column):

    def find(self, sub, start=0, end=None):
        if isinstance(self, (Column)):
            str_array = np.array(self, dtype=str)
            index = np.core.defchararray.find(str_array, sub, start=start, end=end) != -1
            return np.where(index)[0], str_array[index]

        else:
            return NotImplemented


class Table(table.Table):

    # Redefine class attributes (if not, the originals would be taken)
    Column = Column
    MaskedColumn = MaskedColumn
    pl_name = 'name'

    # New functions
    def rename_columns(self, old, new):

        for ko, kn in zip(old, new):
            self.rename_column(ko, kn)

    def nan_to_mask(self):
        """
        Replace nan by masked array
        """
        if self.masked:
            for k in self.keys():
                if self[k].dtype == float:
                    self[k].mask = np.isnan(self[k])
        else:
            raise TypeError("Input must be a Masked Table." +
                            "\n \t Set its mask to True before calling" +
                            " (example: t = Table(t,masked=True)).")

    def by_plName(self, *plName, remove=False):
        """
        Return the complete line of a given planet name (plName)
        """

        position = []
        for pl in plName:
            try:
                position.append(int(self[self.pl_name].find(pl)[0]))
            except TypeError:
                print('Wrong name or incomplete:')
                print(self['name'].find(pl))

        out = self[position]

        if remove and len(position) == 1:
            print(str(*plName) + ' has been removed')
            self.remove_row(int(position[0]))

        return out

    def cols_2_qarr(self, *keys):
        """
        Return column as a quantity array (so can deal with units)
        Can give it many keys.
        """
        out = []
        for k in keys:
            try:
                out.append(np.ma.array(self[k].data) * self[k].unit)
            except TypeError:
                out.append(np.ma.array(self[k].data))

        return tuple(out)

    def new_value(self, plName, col, value):

        names = np.array(self['name'], dtype=str)
        position = np.where(names == plName)[0]

        self[col][position] = value

    def complete(self, right, key=None, join_type='left',
                 add_col=True, metadata_conflicts='warn', **kwargs):
        """
        Add every missing data in self if present in right.

        join_type : 'inner': do not add new rows
                    'outer': add new rows if not present in self
        add_col: add new colums from right
        """

        # Try converting inputs to Table as needed
        if not isinstance(right, Table):
            right = Table(right)

        # If not masked, simple join() will do
        if self.masked:
            out = self._complete(right, key=key, join_type=join_type, add_col=add_col)
        else:
            col_name_map = OrderedDict()
            out = _join(self, right, join_type, col_name_map, keys=key, **kwargs)

        # Merge the column and table meta data. Table subclasses might override
        # these methods for custom merge behavior.
        _merge_table_meta(out, [self, right], metadata_conflicts=metadata_conflicts)

        return out

    def _complete(self, right, key=None, join_type='left', add_col=True):

        if not key:
            raise ValueError('key is empty')

        # Save shared columns without "key"
        cols = intersection(self.keys(), right.keys())
        cols.remove(key)

        # Join tables
        join_t = join(self, right, join_type=join_type, keys=key)

        # Complete masked values of "self" if available in "right"
        for col in cols:

            # Add eventually a condition to check units!

            # Names of joined columns (default from join())
            col1, col2 = col + '_1', col + '_2'

            # Index of masked in "self" and not masked in "right"
            index = np.logical_and(join_t[col1].mask,
                                   np.logical_not(join_t[col2].mask))

            # Reassign value
            join_t[col1].unshare_mask()
            join_t[col1][index] = join_t[col2][index]

            # Remove 2nd column and rename to original
            join_t[col1].name = col
            del join_t[col2]

        # Remove added columns from "right" if not wanted
        if not add_col:
            print('remove non shared columns from second table')
            join_t.remove_columns(difference(self, right))

        return join_t


def difference(left, right):
    if isinstance(left, list) and isinstance(right, list):
        return list(set(left) - set(right))
    else:
        return NotImplemented


def intersection(self, other):
    if isinstance(self, list):
        return list(set(self).intersection(other))
    else:
        return NotImplemented
